use std::{collections::HashMap, env, sync::Arc};

use chrono::{DateTime, Duration, Utc};
use dotenvy::dotenv;
use serenity::{
    async_trait,
    model::{
        application::interaction::{Interaction, InteractionResponseType},
        prelude::{ChannelId, GuildId, MessageId, Reaction, ReactionType, Ready, UserId},
    },
    prelude::{Context, EventHandler, GatewayIntents, TypeMap, TypeMapKey},
    utils::{Color, MessageBuilder},
    Client,
};
use tokio::sync::RwLock;

const GUILD_ID: u64 = 1117346585974677574;
const REACTION_EMOJI: char = '🥺';
const REACTION_LIMIT: u64 = 3;

struct CurrentlyRestarting;

impl TypeMapKey for CurrentlyRestarting {
    type Value = Arc<RwLock<Option<DateTime<Utc>>>>;
}

struct PendingRestartRequests;

impl TypeMapKey for PendingRestartRequests {
    type Value = Arc<RwLock<HashMap<MessageId, DateTime<Utc>>>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);

        let guild_id = GuildId(GUILD_ID);

        GuildId::set_application_commands(&guild_id, &ctx.http, |c| {
            c.create_application_command(|cmd| {
                cmd.name("restart")
                    .description("서버 재시작을 위한 투표를 시작하기")
            })
        })
        .await
        .ok();

        println!("{} is ready!", ready.user.name);
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            if command.data.name == "restart" {
                restart(&ctx, command.channel_id).await;
                command
                    .create_interaction_response(&ctx.http, |r| {
                        r.kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|d| {
                                d.content("서버 재시작을 위한 투표를 시작했어요!")
                            })
                    })
                    .await
                    .ok();
            };
        }
    }

    async fn reaction_add(&self, ctx: Context, add_reaction: Reaction) {
        run_gc(Arc::clone(&ctx.data)).await;

        // skip if not target emoji
        if let ReactionType::Unicode(emoji) = &add_reaction.emoji {
            if emoji != &REACTION_EMOJI.to_string() {
                return;
            }
        } else {
            return;
        }

        // get reaction count
        let count = add_reaction
            .message(&ctx.http)
            .await
            .unwrap()
            .reactions
            .iter()
            .find_map(|r| {
                if let ReactionType::Unicode(emoji) = &r.reaction_type {
                    if *emoji == REACTION_EMOJI.to_string() {
                        Some(r.count)
                    } else {
                        None
                    }
                } else {
                    None
                }
            });

        // skip if less than REACTION_LIMIT reacts
        if let Some(count) = count {
            if count < REACTION_LIMIT {
                return;
            }
        } else {
            return;
        }

        // skip if already restarting
        if ctx
            .data
            .read()
            .await
            .get::<CurrentlyRestarting>()
            .unwrap()
            .read()
            .await
            .is_some()
        {
            return;
        }

        // remove if request exists, return if not
        if ctx
            .data
            .read()
            .await
            .get::<PendingRestartRequests>()
            .unwrap()
            .write()
            .await
            .remove(&add_reaction.message_id)
            .is_none()
        {
            return;
        }

        println!("{}", 1);

        add_reaction
            .message(&ctx.http)
            .await
            .unwrap()
            .reply(&ctx.http, "서버를 재시작할께요!")
            .await
            .unwrap();

        // set restart target
        let restart_target = Utc::now() + Duration::minutes(10);
        ctx.data
            .read()
            .await
            .get::<CurrentlyRestarting>()
            .unwrap()
            .write()
            .await
            .replace(restart_target);

        // run restart
        let success = matches!(
            tokio::process::Command::new("docker")
                .args(["restart", "mc"])
                .status()
                .await
                .map(|s| s.success()),
            Ok(true)
        );

        if !success {
            add_reaction
                .message(&ctx.http)
                .await
                .unwrap()
                .reply(
                    &ctx.http,
                    MessageBuilder::new()
                        .mention(&UserId(323650807541268481))
                        .push(" 서버 재시작에 실패했어요!")
                        .build(),
                )
                .await
                .unwrap();

            return;
        }

        add_reaction
            .message(&ctx.http)
            .await
            .unwrap()
            .reply(&ctx.http, "서버를 성공적으로 암살했어요!")
            .await
            .unwrap();
    }
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment!");

    let intents = GatewayIntents::all();
    let mut client = Client::builder(&token, intents)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<CurrentlyRestarting>(Arc::new(RwLock::new(None)));
        data.insert::<PendingRestartRequests>(Arc::new(RwLock::new(HashMap::new())));
    }

    if let Err(e) = client.start().await {
        println!("Client error: {:?}", e);
    }
}

async fn restart(ctx: &Context, channel_id: ChannelId) {
    run_gc(Arc::clone(&ctx.data)).await;

    if let Some(restart_target) = ctx
        .data
        .read()
        .await
        .get::<CurrentlyRestarting>()
        .expect("Expected CurrentlyRestarting in TypeMap.")
        .read()
        .await
        .as_ref()
    {
        channel_id
            .say(
                &ctx.http,
                &format!(
                    "이미 서버가 재시작되는 중이에요! 최대 {}초 예상",
                    (*restart_target - Utc::now()).num_seconds()
                ),
            )
            .await
            .ok();
        return;
    }

    let msg = if let Ok(msg) = channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.color(Color::RED);
                e.title("서버가 죽었나요?! 😱😱😱");
                e.description(
                    &format!("🫠 저런! 이 메시지에 달린 {REACTION_EMOJI} 리액션의 수가 {REACTION_LIMIT}을 넘으면 서버를 재시작할께요!"),
                );
                e.footer(|f| f.text("이 메시지는 10분동안만 유효해요!"));
                e
            })
        })
        .await
    {
        msg
    } else {
        return;
    };

    msg.react(&ctx.http, REACTION_EMOJI).await.ok();

    run_gc(Arc::clone(&ctx.data)).await;
    ctx.data
        .read()
        .await
        .get::<PendingRestartRequests>()
        .expect("Expected PendingRestartRequests in TypeMap.")
        .write()
        .await
        .insert(msg.id, Utc::now());
}

async fn run_gc(data: Arc<RwLock<TypeMap>>) {
    let data = data.read().await;
    let currently_restarting = data.get::<CurrentlyRestarting>().unwrap();
    if let Some(restart_target) = currently_restarting.read().await.as_ref() {
        if restart_target < &Utc::now() {
            *currently_restarting.write().await = None;
        }
    }
    let pending_restart_requests = data.get::<PendingRestartRequests>().unwrap();
    let mut old = Utc::now() - Duration::minutes(10);
    pending_restart_requests
        .write()
        .await
        .retain(|_, v| v >= &mut old);
}
